import React, { Component } from "react";
import { View, Text, StyleSheet, ImageBackground, Dimensions, Picker, TouchableOpacity, FlatList, Image } from "react-native";
import FontAwesomeIcon from "react-native-vector-icons/FontAwesome";
import FeatherIcon from "react-native-vector-icons/Feather";

const SCREEN_WIDTH = Dimensions.get('screen').width;
const DATA = [
  {
    id: '1',
    title: 'Hot Coffees',
  },
  {
    id: '2',
    title: 'Hot Teas',
  },
  {
    id: '3',
    title: 'Hot Drinks',
  },
  {
    id: '4',
    title: 'Cappuccino Blended Beverages',
  },
  {
    id: '5',
    title: 'Cold Coffees',
  },
  {
    id: '6',
    title: 'Third Item',
  },
  {
    id: '7',
    title: 'First Item',
  },
];

function Item({ title }) {
  return (
    <View style={styles.item}>
      <Image source={require('./assets/coffeeImage.png')} style={{height: 75, width: 75}}/>
      <Text style={{fontSize: 18, maxWidth: 200 }}>{title}</Text>
    </View>
  );
}

export default class App extends Component{
  render(){
    return(
      <ImageBackground resizeMode={"stretch"} style={ styles.imgBackground } source={require('./assets/CoffeeCover.png')}>
        <View style={styles.converContainer}>
            <View style={{flexDirection:'row'}}>
              <Text style={styles.txtMyReward}>My Rewards</Text>
              <Text style={styles.txtPickup}>Pickup at</Text>
            </View>
            <View style={{flexDirection:'row', justifyContent:'space-around'}}>
              <TouchableOpacity>
                <Text style={styles.txtReward}>$2.00</Text>
              </TouchableOpacity>
              <View style={{borderRadius: 20, borderWidth: 0, borderColor: '#ccc', overflow: 'hidden', marginRight: 10}}>
                <Picker
                  style={{ height: 40, width: 250, backgroundColor:'#fff'}} mode={"dialog"}>
                <Picker.Item label="Select" value="java" />
                <Picker.Item label="JavaScript" value="js" />
                </Picker>
              </View>
            </View>
            <View style={{}}>
              <View style={{flexDirection:'row', alignItems:'center', marginTop: 20}}>
                <TouchableOpacity>
                  <Text style={{  fontWeight:'bold', borderRadius: 7, textAlign:'center', paddingHorizontal: 5, width: 75, backgroundColor:'#06f929', paddingVertical: 5, color:'#000', marginLeft: 30}}>Specials</Text>
                </TouchableOpacity>
                <FeatherIcon name="arrow-right" style={{ paddingLeft: 5, fontWeight:'bold'}} size={20} color="#fff" />
              </View>
              <Text style={{margin: 5, color:'#fff', marginLeft: 30, fontSize: 20, fontWeight:'bold'}}>Staff favourites</Text>
              <Text style={{margin: 5, color:'#fff', marginLeft: 30, fontSize: 20, fontWeight:'bold'}}>and Exclusive</Text>
            </View>
        </View>
        <View style={styles.itemListContainer}>
          <View style={{ alignItems: 'center', backgroundColor:'#f0f0f0', height: 50, borderTopLeftRadius: 40, borderTopRightRadius: 40, flexDirection:'row', justifyContent:'space-around'}}>
            <TouchableOpacity><Text>Menu</Text></TouchableOpacity>
            <TouchableOpacity><Text>Favorites</Text></TouchableOpacity>
            <TouchableOpacity><Text>Previous</Text></TouchableOpacity>
            <TouchableOpacity><FeatherIcon name="search" style={{marginTop: 5, fontWeight:'bold'}} size={20} color="#000" /></TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row', justifyContent:'space-between'}}>
            <Text style={{ color:'#e88131', fontWeight:'bold', fontSize: 22, paddingLeft: 22, paddingTop:20, marginLeft: 12}}>Drinks</Text>
            <TouchableOpacity><Text style={{ paddingRight: 22, paddingTop:20, marginLeft: 12}}>See all 156</Text></TouchableOpacity>
          </View>
          <FlatList
            data={DATA}
            renderItem={({ item }) => <Item title={item.title} />}
            keyExtractor={item => item.id}
          />
        </View>
        <View style={styles.menuContainer}>
          <View style={{height:'100%', borderTopEndRadius: 40, borderTopLeftRadius: 40, alignItems: 'center', backgroundColor:'#0f384c', flexDirection:'row', justifyContent:'space-around', paddingHorizontal: 20}}>
            <TouchableOpacity style={{paddingVertical: 13, paddingHorizontal: 15, borderRadius: 40, backgroundColor:'#f19651'}}><FontAwesomeIcon name="coffee" style={{marginTop: 5, fontWeight:'bold'}} size={20} color="#fff" /></TouchableOpacity>
            <TouchableOpacity><FontAwesomeIcon name="list" style={{marginTop: 5, fontWeight:'bold'}} size={20} color="#fff" /></TouchableOpacity>
            <TouchableOpacity><FeatherIcon name="heart" style={{marginTop: 5, fontWeight:'bold'}} size={20} color="#fff" /></TouchableOpacity>
            <TouchableOpacity>
              <FeatherIcon name="shopping-cart" style={{marginTop: 5, fontWeight:'bold'}} size={20} color="#fff" />
              <Text style={{backgroundColor:'#09f529', position:'absolute', borderRadius: 20, paddingVertical: 2, paddingHorizontal: 5, left: 15, top: 0,fontSize: 10 }}>3</Text>
            </TouchableOpacity>
            <TouchableOpacity><FontAwesomeIcon name="user-circle-o" style={{marginTop: 5, fontWeight:'bold'}} size={20} color="#fff" /></TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    )
  }
}


const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent:'center',
    alignItems:'center'
  },
  converContainer:{
    flex: 3
  },
  itemListContainer:{
    flex: 5,
    backgroundColor:'#fff',
    borderTopLeftRadius: 38,
    borderTopRightRadius: 38,
    width:'100%',
  },
  menuContainer:{
    flex: 1,
    backgroundColor:'#fff',
    width:'100%',
  },
  txtReward:{
    margin: 5,
    backgroundColor:'#fff',
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 15,
    fontWeight:'bold'
  },
  item: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginHorizontal: 16,
    flexDirection:'row',
    alignItems:'center',
  },
  txtMyReward:{
    margin: 5,
    color:'#fff',
    paddingLeft: 10
  },
  txtPickup:{
    marginVertical:5,
    marginLeft: 10,
    color:'#fff'
  },
  imgBackground:{
    flex:1,
  }
})